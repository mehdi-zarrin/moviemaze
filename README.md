## About The project
Please follow these steps to run the project
- clone the project
- in your terminal: 

``` docker-compose up --build -d && docker exec -it fashionette_web_server composer install ```


- **[Open in browser](http://127.0.0.1:8084/?q=deadwood)**
## Test

- to run the test suite without hitting the real api:
 
``` docker exec -it fashionette_web_server vendor/bin/phpunit --exclude-group api ```

- to run all tests

``` docker exec -it fashionette_web_server vendor/bin/phpunit ```

## Third-party packages
- **[Data transfer objects with batteries included](https://github.com/spatie/data-transfer-object)**

## Further notes
The main controller 'MoviesController' has a dependency of 'MoviesApiContract' which is an interface
this interface is currently bind to MovieMaze class concrete implementation, but later if we decide to
use another provider for example imdb api we can create another class which is adhere to the same contract
and swap the implementation in the AppServiceProvider.( D in SOLID, Dependency inversion )

To have an unified data throughout the application DTO are used to handle request and response.
since there are no specification regarding the response structure, I just assume the most useful data 
in the response DTO.

