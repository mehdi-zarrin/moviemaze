<?php

namespace App\Providers;

use App\Fashionette\ApiProviders\MoviesApiContract;
use App\Fashionette\ApiProviders\MovieMaze;
use App\Fashionette\ApiProviders\Transformers\MovieMazeTransformer;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(MoviesApiContract::class, function() {
            return new MovieMaze(new MovieMazeTransformer());
        });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
