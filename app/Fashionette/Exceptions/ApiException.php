<?php
namespace App\Fashionette\Exceptions;
class ApiException extends \Exception
{
    public function render()
    {
        return response()->json([

            'message' => $this->getMessage(),
            'status'  => $this->getCode()

        ])->setStatusCode($this->getCode());
    }
}
