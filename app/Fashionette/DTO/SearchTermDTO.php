<?php
namespace App\Fashionette\DTO;
use Illuminate\Http\Request;
use Spatie\DataTransferObject\DataTransferObject;

class SearchTermDTO extends DataTransferObject
{
    public string $query;

    public static function fromRequest(Request $request)
    {
        return new self([
            'query' => strtolower($request->input('q'))
        ]);
    }

}
