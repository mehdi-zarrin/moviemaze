<?php
namespace App\Fashionette\DTO;
use Spatie\DataTransferObject\DataTransferObject;

class MovieDTO extends DataTransferObject
{
    public string $name;

    public ?int $duration;

    public float $rating;

    public string $language;

    public ?string $official_site;

    public ?array $genres;

    public ?string $cover_image;

    public ?string $thumbnail_image;

}
