<?php

namespace App\Fashionette\ApiProviders;
use App\Fashionette\ApiProviders\Transformers\TransformerContract;
use App\Fashionette\Exceptions\ApiException;
use Illuminate\Http\Client\RequestException;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Http;
use Spatie\DataTransferObject\DataTransferObject;

class MovieMaze implements MoviesApiContract
{
    const API_URL = 'http://api.tvmaze.com/search/shows';
    const CACHE_LIFE_TIME = 420;

    private TransformerContract $transformer;

    public function __construct(TransformerContract $transformer)
    {
        $this->transformer = $transformer;
    }

    public function get(DataTransferObject $searchTermDto)
    {
        return Cache::remember(
            $searchTermDto->query,
            static::CACHE_LIFE_TIME,
            function() use ($searchTermDto) {

                try {

                    $data = Http::get(self::API_URL . '?q=' . $searchTermDto->query)
                        ->throw()
                        ->json();

                } catch (RequestException $e) {

                    throw new ApiException($e->getMessage(), $e->getCode());

                }

                if (count($data) === 0) {
                    return $data;
                }

                return collect($data)
                    ->map(

                        fn($item) => $this->transformer->transform($item)->toArray()

                    )->filter(function($item) use ($searchTermDto) {

                        return strtolower($item['name']) == $searchTermDto->query;

                    });
            }
        );

    }
}
