<?php
namespace App\Fashionette\ApiProviders\Transformers;
use App\Fashionette\DTO\MovieDTO;
use Spatie\DataTransferObject\DataTransferObject;

class MovieMazeTransformer implements TransformerContract
{
    public function transform($data): DataTransferObject
    {
        $show = $data['show'];
        return new MovieDTO([
            'name' => $show['name'],
            'duration' => $show['runtime'],
            'rating' => (float) $show['rating']['average'],
            'official_site' => $show['officialSite'],
            'language' => $show['language'],
            'genres' => $show['genres'],
            'cover_image' => optional($show['image'])['original'],
            'thumbnail_image' => optional($show['image'])['medium']
        ]);
    }
}
