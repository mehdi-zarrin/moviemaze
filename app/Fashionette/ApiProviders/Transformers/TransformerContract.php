<?php

namespace App\Fashionette\ApiProviders\Transformers;
use Spatie\DataTransferObject\DataTransferObject;

interface TransformerContract
{
    public function transform($data): DataTransferObject;
}
