<?php
namespace App\Fashionette\ApiProviders;

use Spatie\DataTransferObject\DataTransferObject;

interface MoviesApiContract
{
    public function get(DataTransferObject $transferObject);
}
