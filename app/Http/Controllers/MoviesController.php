<?php

namespace App\Http\Controllers;
use App\Fashionette\ApiProviders\MoviesApiContract;
use App\Fashionette\DTO\SearchTermDTO;
use Illuminate\Http\Client\RequestException;
use Illuminate\Http\Request;

class MoviesController extends Controller
{
    public function index(Request $request, MoviesApiContract $api)
    {
        
        return $api->get(SearchTermDTO::fromRequest($request)); 
        
    }

}
