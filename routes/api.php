<?php

use App\Fashionette\Exceptions\ApiException;
use Illuminate\Support\Facades\Route;
use Symfony\Component\HttpFoundation\Response;

Route::get('/', 'MoviesController@index');

Route::fallback(function(){
    throw new ApiException('Not found', Response::HTTP_NOT_FOUND);
});

