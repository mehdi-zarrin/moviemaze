<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Http;
use Tests\TestCase;

class generalApiTest extends TestCase
{
    /**
     * @test
     */
    public function other_endpoints_should_return_a_not_found_exception()
    {
        $this->getJson('/some-non-existing-end-point')
            ->assertStatus(404)
            ->assertJsonStructure([
                'message',
                'status'
            ]);
    }
}
