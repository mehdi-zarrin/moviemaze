<?php

namespace Tests\Feature;
use App\Fashionette\ApiProviders\MovieMaze;
use App\Fashionette\ApiProviders\MoviesApiContract;
use App\Fashionette\DTO\MovieDTO;
use App\Fashionette\Exceptions\ApiException;
use Illuminate\Http\Client\RequestException;
use Illuminate\Http\Client\Response;
use Illuminate\Support\Facades\Http;
use ReflectionProperty;
use Tests\TestCase;

class MovieMazeApiTest extends TestCase
{

    protected function setUp(): void
    {
        parent::setUp();
        // if we are decided in the service provider we wanted to use another provider instead of movieMaze
        // then there is no point to run the tests in this class
        if (!resolve(MoviesApiContract::class) instanceof MovieMaze) {
            $this->markTestSkipped();
        }
    }


    /**
     * @test
     */
    public function it_should_return_response_ok()
    {
        $this->fakeRequest();
        $response = $this->getJson('/');
        $response->assertStatus(200);
    }

    /**
     * @test
     * @group api
     */
    public function test_against_the_real_api()
    {
        $this->getJson('/?q=Deadwood')
            ->assertJsonStructure([
                $this->getMovieDTOStructure()
            ]);
    }

    /**
     * @test
     */
    public function it_should_return_empty_result_if_name_is_not_correct()
    {
        $this->fakeRequest();
        $result = $this->getJson('/?q=dead')
            ->json();

        $this->assertCount(0, $result);
    }


    /**
     * @test
     */
    public function it_should_have_proper_structure()
    {
        $this->fakeRequest();

        $this->getJson('/?q=Deadwood')
            ->assertJsonStructure([
                $this->getMovieDTOStructure()
            ]);
    }

    /**
     * @test
     */
    public function it_should_only_have_one_item_per_search()
    {
        $this->fakeRequest();

        $this->getJson('/?q=DeadWood')
            ->assertJson([
                [
                    'name' => 'DeadWood'
                ]
            ]);
    }

    /**
     * @test
     */
    public function it_can_handle_exception_in_case_there_is_problem_in_the_api()
    {
        $this->fakeApiFault();
        $this->getJson('/?q=deadwood')->assertJsonStructure([
            'message',
            'status'
        ])->assertStatus(500);
    }


    private function fakeRequest()
    {
        Http::fake([
            'http://api.tvmaze.com/search/shows?q=*' => Http::response($this->getFakeData(), 200)
        ]);
    }

    private function fakeApiFault($code = 500)
    {
        Http::fake([
            'http://api.tvmaze.com/search/shows?q=*' => Http::response([], $code)
        ]);
    }


    private function getMovieDTOStructure()
    {
        $reflector = new \ReflectionClass(MovieDTO::class);
        $properties = $reflector->getProperties(ReflectionProperty::IS_PUBLIC);
        $structure = [];

        foreach ($properties as $property) {
            $structure[] = $property->getName();
        }

        return $structure;
    }

    private function getFakeData()
    {
        return [
            [
                "score" => 17.330563,
                "show" => [
                    "id" => 139,
                    "url" => "http => //www.tvmaze.com/shows/139/girls",
                    "name" => "DeadWood",
                    "type" => "Scripted",
                    "language" => "English",
                    "rating" => [
                        "average" => 9.7
                    ],
                    "genres" => [
                        "Drama",
                        "Romance"
                    ],
                    "status" => "Ended",
                    "runtime" => 30,
                    "premiered" => "2012-04-15",
                    "officialSite" => "http => //www.hbo.com/girls",
                    "schedule" => [
                        "time" => "22:00",
                        "days" => [
                            "Sunday"
                        ]
                    ],
                    "image" => [
                        "medium" => "http://static.tvmaze.com/uploads/images/medium_portrait/4/11724.jpg",
                        "original" => "http://static.tvmaze.com/uploads/images/original_untouched/4/11724.jpg"
                    ]
                ]
            ],
            [

                "score" => 17.330563,
                "show" => [
                    "id" => 139,
                    "url" => "http => //www.tvmaze.com/shows/139/girls",
                    "name" => "DeadPool",
                    "type" => "Scripted",
                    "language" => "English",
                    "genres" => [
                        "Drama",
                        "Romance"
                    ],
                    "rating" => [
                        "average" => 9.7
                    ],
                    "status" => "Ended",
                    "runtime" => 30,
                    "premiered" => "2012-04-15",
                    "officialSite" => "http => //www.hbo.com/girls",
                    "schedule" => [
                        "time" => "22:00",
                        "days" => [
                            "Sunday"
                        ]
                    ],
                    "image" => [
                        "medium" => "http://static.tvmaze.com/uploads/images/medium_portrait/4/11724.jpg",
                        "original" => "http://static.tvmaze.com/uploads/images/original_untouched/4/11724.jpg"
                    ]
                ]

            ]

        ];
    }
}
